#! /usr/bin/env php
<?php

use Codecritics\SayHelloCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

require 'vendor/autoload.php';

$app = new Application("Codecritics PHP-CLI", "1.0");
/**
 * La commande sayHelloTo prend un argument
 * et execute la closure dans setCode
 */

$app->add(new SayHelloCommand());
/**
 * Après avoir rendu le fichier excecutable
 * avec chmod on execute la commande.
 */
$app->run();